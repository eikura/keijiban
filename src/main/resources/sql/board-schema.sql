DROP TABLE IF EXISTS `board`;
CREATE TABLE `board` (
`post_id` INT AUTO_INCREMENT,
`uid` bigint(20) unsigned NOT NULL,
`display_name` varchar(12) NOT NULL,
`title` varchar(200),
`content` varchar(3000),
`time_written` timestamp DEFAULT CURRENT_TIMESTAMP ,
`time_edited` timestamp DEFAULT CURRENT_TIMESTAMP ,
`read_count` bigint(20),
`tag_one` varchar(50),
`tag_two` varchar(50),
`tag_three` varchar(50),
FULLTEXT (`title`, `content`),
PRIMARY KEY(`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;