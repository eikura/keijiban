
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
`fid` INT NOT NULL AUTO_INCREMENT,
`pid` INT NOT NULL,
`original_file_name` VARCHAR(260) NOT NULL,
`stored_file_name` VARCHAR(36) NOT NULL,
`file_size` INT NULL DEFAULT NULL ,
`crea_dtm` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

