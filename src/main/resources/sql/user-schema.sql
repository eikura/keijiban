
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
`uid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
`name` varchar(12) NOT NULL,
`password` binary(16) NOT NULL,
`display_name` varchar(12) NOT NULL,
`admin_level` int(1) NOT NULL,
`time_join` timestamp DEFAULT CURRENT_TIMESTAMP,
`time_edited` timestamp DEFAULT CURRENT_TIMESTAMP,
`time_last_login` timestamp DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY(`uid`,`name`,`display_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;