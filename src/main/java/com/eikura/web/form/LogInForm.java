package com.eikura.web.form;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogInForm {
    String userName;
    String password;
}
