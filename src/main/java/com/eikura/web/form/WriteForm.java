package com.eikura.web.form;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WriteForm {

    private String title;
    private String content;
    private String tagOne;
    private String tagTwo;
    private String tagThree;

}
