package com.eikura.web.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PageForm {
    int pageStart;
    int perPageNum;
}
