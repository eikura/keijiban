package com.eikura.web.form;


import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SignUpForm {
    String userName;
    String password;
    String displayName;
}
