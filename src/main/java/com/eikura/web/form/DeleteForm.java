package com.eikura.web.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DeleteForm {
    int loginId;
    int deleteId;
    int adminLevel;
}
