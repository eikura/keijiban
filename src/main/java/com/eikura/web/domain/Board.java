package com.eikura.web.domain;

import lombok.*;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.sql.Timestamp;


@Alias("board")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
@Builder
public class Board implements Serializable {

    private int postId;
    private int uId;
    private String displayName;
    private String title;
    private String content;
    private Timestamp timeWritten;
    private Timestamp timeEdited;
    private int readCount;
    private String tagOne;
    private String tagTwo;
    private String tagThree;

}
