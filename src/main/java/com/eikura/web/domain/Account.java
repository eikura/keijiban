package com.eikura.web.domain;

import lombok.*;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;
import java.sql.Timestamp;

@Alias("account")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
@Builder
public class Account implements Serializable {

    private int uId;
    private String name;
    private String password;
    private String displayName;
    private int adminLevel;
    private Timestamp timeJoined;
    private Timestamp timeEdited;
    private Timestamp timeLastLogin;

}