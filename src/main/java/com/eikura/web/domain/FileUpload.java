package com.eikura.web.domain;


import lombok.*;
import org.apache.ibatis.type.Alias;

import java.sql.Timestamp;

@Alias("fileUpload")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
@Builder
public class FileUpload {

    private int fid;
    private int pid;
    private String originalFileName;
    private String storedFileName;
    private int fileSize;
    private Timestamp creaDtm;

}

