package com.eikura.web.controller;


import com.eikura.web.service.BoardService;
import com.eikura.web.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class HomeController {

    @Autowired
    BoardService boardService;

    @Autowired
    AccountService accountService;

    @RequestMapping(value = "/")
    public String home(
            @CookieValue(value = "adminLevel", defaultValue = "-1") int adminLevel,
            @CookieValue(value = "userName", defaultValue = "") String userName,
            @CookieValue(value = "uId", defaultValue = "-1") int uId,
            ModelMap modelMap, final HttpSession httpSession, HttpServletRequest httpServletRequest){

        if(adminLevel != -1 & !userName.equals("") & uId != -1){
            httpSession.setAttribute("adminLevel", adminLevel);
            httpSession.setAttribute("userName", userName);
            httpSession.setAttribute("uId", uId);
        }

        if(httpSession.getAttribute("returnURL") != "/"){
            String domain = httpServletRequest.getScheme() + "://" + httpServletRequest.getServerName() + ":" + httpServletRequest.getServerPort();
            String returnURL = httpServletRequest.getRequestURL().toString().replace(domain, "");
            httpSession.setAttribute("returnURL", returnURL);
        }

        return "index";
//        return "index_new";
    }
}
