package com.eikura.web.controller;

import com.eikura.web.domain.Board;
import com.eikura.web.form.PageForm;
import com.eikura.web.service.BoardService;
import com.eikura.web.validation.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class PageController {

    @Autowired
    BoardService boardService;

//    @Autowired
//    Page page;

    @RequestMapping(value = "/page")
    public String displayPosts(Model m, @RequestParam(defaultValue = "1") int pageIndex, HttpSession httpSession, HttpServletRequest httpServletRequest){
        Page page = new Page(boardService.getPostNum(), pageIndex);
//        page.setPage(pageIndex);
//        page.setPerPageNum(10);
//        page.setLimitPageNum(boardService.getPostNum());
        PageForm pageForm = new PageForm(page.getStartIndex(), page.getPageSize());
        List<Board> posts = boardService.getPostsByPageIdx(pageForm);

        String domain = httpServletRequest.getScheme() + "://" + httpServletRequest.getServerName() + ":" + httpServletRequest.getServerPort();
        String returnURL = httpServletRequest.getRequestURL().toString().replace(domain, "");
        httpSession.setAttribute("returnURL", returnURL);
        httpSession.setAttribute("page", pageIndex);

        m.addAttribute("paging", page);
        m.addAttribute("posts", posts);
        m.addAttribute("currentPage", page.getCurPage());
        m.addAttribute("leftPage", page.getPrevPage());
        m.addAttribute("rightPage", page.getNextPage());
        m.addAttribute("minPage", page.getStartPage());
        m.addAttribute("maxPage", page.getEndPage());

        return "board";
    }
}
