package com.eikura.web.controller;

import com.eikura.web.form.LogInForm;
import com.eikura.web.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @Autowired
    LoginService loginService;

    @RequestMapping("login")
    public String login(Model m, HttpSession httpSession){
        if(httpSession.getAttribute("returnURL") == null){
            httpSession.setAttribute("returnURL", "/");
        }
        m.addAttribute("loginAttr", new LogInForm());
        return "login";
    }

    @RequestMapping("login/test")
    public String loginTest(Model m){
        m.addAttribute("loginTestAttr", new LogInForm());
        return "login_test";
    }

    @RequestMapping(value = "/login/try", method = RequestMethod.POST)
    public String signIn(@ModelAttribute LogInForm logInForm, HttpSession httpSession, HttpServletResponse httpServletResponse){

        boolean loginResult = loginService.loginCheck(logInForm, httpSession, httpServletResponse);
        System.out.println(loginResult);
        if(loginResult){
            return "redirect:" + httpSession.getAttribute("returnURL");
        }else{
            return "redirect:/login";
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String signOut(HttpSession httpSession, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        String returnURL = (String)httpSession.getAttribute("returnURL");
        loginService.logout(httpSession, httpServletRequest, httpServletResponse);
        return "redirect:" + returnURL;
    }
}