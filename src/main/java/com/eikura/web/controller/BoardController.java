package com.eikura.web.controller;


import com.eikura.web.domain.Board;
import com.eikura.web.form.LogInForm;
import com.eikura.web.form.WriteForm;
import com.eikura.web.service.AccountService;
import com.eikura.web.service.BoardService;
import com.eikura.web.service.FileUploadService;
import com.eikura.web.service.LoginService;
import com.eikura.web.util.FileUtils;
import com.eikura.web.validation.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class BoardController {

    @Autowired
    BoardService boardService;

    @Autowired
    LoginService loginService;

    @Autowired
    AccountService accountService;

    @Autowired
    FileUploadService fileUploadService;


//    @Autowired
//    Page page;

    @RequestMapping("/board")
    public String boardHome(HttpSession httpSession){
        httpSession.setAttribute("page", 1);
        return "redirect:/page?pageIndex=1";
    }


    @RequestMapping("/edit/{pid}")
    public String edit(Model m, @PathVariable int pid, HttpSession httpSession){

        if(isUser(httpSession)){
            if(httpSession.getAttribute("writerId") == httpSession.getAttribute("uId")){
                WriteForm writeForm = new WriteForm();
                writeForm.setTitle((String)httpSession.getAttribute("previousTitle"));
                writeForm.setContent((String)httpSession.getAttribute("previousContent"));
                writeForm.setTagOne((String)httpSession.getAttribute("previousTagOne"));
                writeForm.setTagTwo((String)httpSession.getAttribute("previousTagTwo"));
                writeForm.setTagThree((String)httpSession.getAttribute("previousTagThree"));

                m.addAttribute("editAttr", writeForm);
                m.addAttribute("pid", pid);
                return "edit";
            }else{
                return "redirect:/read/" + pid;
            }
        }else{
            httpSession.setAttribute("returnURL", "/");
            return forward2Login(m);
        }
    }

    @RequestMapping("/edit/{pid}/try")
    public String editPost(Model m, @PathVariable int pid, @ModelAttribute WriteForm writeForm, HttpSession httpSession){
        if(isUser(httpSession)){
            Board editedPost = buildNewPost(writeForm);
            editedPost.setPostId(pid);
            boardService.editPost(editedPost);
            return "redirect:/read/" + pid;
        }
        else{
            httpSession.setAttribute("returnURL", "/");
            return forward2Login(m);
        }
    }

    @RequestMapping("/write")
    public String write(Model m, HttpSession httpSession){
        if (isUser(httpSession)){
            m.addAttribute("writeAttr", new WriteForm());
            return "write";
        }
        else{
            httpSession.setAttribute("returnURL", "/write");
            return forward2Login(m);
        }
    }

    @RequestMapping(value = "/write/try")
    public String writePost(Model m, @ModelAttribute WriteForm writeForm, HttpSession httpSession, HttpServletRequest httpServletRequest) throws Exception{
        if(isUser(httpSession)){
            Board newPost = buildNewPost(writeForm);
            newPost.setDisplayName(accountService.getNickNameByUId((Integer)httpSession.getAttribute("uId")));
            newPost.setUId(accountService.getUIdByUserName((String)httpSession.getAttribute("userName")));
            boardService.writePost(newPost);
            fileUploadService.uploadFiles(newPost.getPostId(), httpServletRequest);
            return "redirect:/page/1";
        }
        else{
            httpSession.setAttribute("returnURL", "/");
            return forward2Login(m);
        }
    }

//    private void fileHandler(MultipartHttpServletRequest multipartHttpServletRequest){
//        Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
//        MultipartFile multipartFile = null;
//        while(iterator.hasNext()){
//            multipartFile = multipartHttpServletRequest.getFile(iterator.next());
//            if(multipartFile.isEmpty() == false){
//                System.out.println("----------- file start -----------");
//                System.out.println("name: " + multipartFile.getName());
//                System.out.println("filename: " + multipartFile.getOriginalFilename());
//                System.out.println("size: " + multipartFile.getSize());
//                System.out.println("----------- file end -----------");
//            }
//        }
//    }


    @RequestMapping(value = "/read/{pid}", method = RequestMethod.GET)
    public String readPost(Model m, @PathVariable int pid, HttpSession httpSession, HttpServletRequest httpServletRequest){
        Board post = boardService.getPostByPostId(pid);
        List<String> images = fileUploadService.getImagesByPid(pid);

        images = images.stream()
                .map( img -> "/resources/CDN/" + img)
                .collect(Collectors.toList());

        m.addAttribute("images", images);

        m.addAttribute("post", post);
        m.addAttribute("pid", pid);
        m.addAttribute("page", httpSession.getAttribute("page"));
        m.addAttribute("shareURL", httpServletRequest.getRequestURL());
//        m.addAttribute("previous", post);
        httpSession.setAttribute("writerId", post.getUId());
        httpSession.setAttribute("previousTitle", post.getTitle());
        httpSession.setAttribute("previousContent", post.getContent());
        httpSession.setAttribute("previousTagOne", post.getTagOne());
        httpSession.setAttribute("previousTagTwo", post.getTagTwo());
        httpSession.setAttribute("previousTagThree", post.getTagThree());

        boardService.updateReadCount(pid);

        String domain = httpServletRequest.getScheme() + "://" + httpServletRequest.getServerName() + ":" + httpServletRequest.getServerPort();
        String returnURL = httpServletRequest.getRequestURL().toString().replace(domain, "");
        httpSession.setAttribute("returnURL", returnURL);

        return "read";
    }

    @RequestMapping(value = "/deletePost/{pid}")
    public String deletePost(Model m, @PathVariable int pid, HttpSession httpSession){
        if (httpSession.getAttribute("uId") != null){
            boardService.deletePost((Integer)httpSession.getAttribute("uId"), pid, (Integer)httpSession.getAttribute("adminLevel"));
            return "redirect:/page/1";
        }else{
            return "redirect:/page/1";
        }

    }

    private Board buildNewPost(WriteForm writeForm){
        Board newPost = new Board();

        newPost.setTitle(writeForm.getTitle());
        newPost.setContent(writeForm.getContent());
        newPost.setTagOne(writeForm.getTagOne());
        newPost.setTagTwo(writeForm.getTagTwo());
        newPost.setTagThree(writeForm.getTagThree());
        newPost.setReadCount(0);
        Timestamp now = new Timestamp(System.currentTimeMillis());
        newPost.setTimeEdited(now);
        newPost.setTimeWritten(now);

        return newPost;
    }

    private String forward2Login(Model m){
        m.addAttribute("loginAttr", new LogInForm());
        return "login";
    }

    private boolean isUser(HttpSession httpSession){
        return httpSession.getAttribute("userName") != null;
    }
}
