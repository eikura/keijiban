package com.eikura.web.controller;


import com.eikura.web.domain.Account;
import com.eikura.web.form.SignUpForm;
import com.eikura.web.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;

@Controller
public class AccountController {

    @Autowired
    AccountService accountService;

    @RequestMapping(value = "signUp")
    public String signPage(Model m){
        m.addAttribute("signUpAttr", new SignUpForm());
        return "sign";
    }

    @RequestMapping(value = "/signUp/try")
    public String signUp(@ModelAttribute SignUpForm signUpForm){
        Account newAccount = buildNewAccount(signUpForm);
        accountService.signUp(newAccount);
        return "redirect:/";
    }

    @RequestMapping(value = "/deleteAccount/{id}", method = RequestMethod.GET)
    public String deleteAccount(@PathVariable int id, HttpSession httpSession){
        accountService.deleteAccount(id);
        httpSession.invalidate();
        return "redirect:/";
    }

    @RequestMapping(value = "/userInfo")
    public String userInfo(Model m, HttpSession httpSession){
        if(httpSession.getAttribute("uId") != null)
        {
            m.addAttribute("userInfo", accountService.getUserInfoByUId((Integer)httpSession.getAttribute("uId")));
            return "userInfo";
        }
        else{
            return "redirect:/";
        }

    }

    private Account buildNewAccount(SignUpForm signUpForm){
        Account newAccount = new Account();

        newAccount.setName(signUpForm.getUserName());
        newAccount.setPassword(signUpForm.getPassword());
        newAccount.setDisplayName(signUpForm.getDisplayName());
        newAccount.setAdminLevel(3);
        Timestamp now = new Timestamp(System.currentTimeMillis());
        newAccount.setTimeEdited(now);
        newAccount.setTimeJoined(now);
        newAccount.setTimeLastLogin(now);

        return newAccount;
    }
}
