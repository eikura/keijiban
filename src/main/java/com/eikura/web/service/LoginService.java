package com.eikura.web.service;

import com.eikura.web.dao.AccountDao;
import com.eikura.web.dao.LoginDao;
import com.eikura.web.form.LogInForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CookieValue;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;

@Service
public class LoginService {

    @Resource
    LoginDao loginDao;

    @Resource
    AccountDao accountDao;

    public boolean loginCheck(LogInForm logInForm, HttpSession httpSession, HttpServletResponse httpServletResponse){
        boolean checkResult = loginDao.loginCheck(logInForm);
        if(checkResult){

            String userName = logInForm.getUserName();
            int uId = accountDao.getUIdByUserName(userName);
            int adminLevel = accountDao.getAdminLevelByUId(uId);
            loginDao.updateLastLogin(uId);
            httpSession.setAttribute("adminLevel", adminLevel);
            httpSession.setAttribute("userName", userName);
            httpSession.setAttribute("uId", uId);

            String cookiePath = "/";
            int cookieLifeTime = 100000;

            Cookie cookie = new Cookie("adminLevel", String.valueOf(adminLevel));
            cookie.setPath(cookiePath);
            cookie.setMaxAge(cookieLifeTime);
            httpServletResponse.addCookie(cookie);
            cookie = new Cookie("userName", userName);
            cookie.setPath(cookiePath);
            cookie.setMaxAge(cookieLifeTime);
            httpServletResponse.addCookie(cookie);
            cookie = new Cookie("uId", String.valueOf(uId));
            cookie.setPath(cookiePath);
            cookie.setMaxAge(cookieLifeTime);
            httpServletResponse.addCookie(cookie);

        }
        return checkResult;
    }

    public void logout(HttpSession httpSession, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        destroyCookies(httpServletRequest, httpServletResponse);
        httpSession.invalidate();
    }

    private void destroyCookies(HttpServletRequest httpServletRequest, HttpServletResponse httpServletReponse){
        Cookie[] cookies = httpServletRequest.getCookies();
        if(cookies != null){
            Arrays.stream(cookies)
                    .forEach(c -> {
                        c.setMaxAge(0);
                        c.setPath("/");
                        c.setValue(null);
                        httpServletReponse.addCookie(c);
                    });
        }
    }

}
