package com.eikura.web.service;

import com.eikura.web.dao.BoardDao;
import com.eikura.web.dao.MetaDao;
import com.eikura.web.domain.Board;
import com.eikura.web.form.DeleteForm;
import com.eikura.web.form.PageForm;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BoardService {

    @Resource
    BoardDao boardDao;

    @Resource
    MetaDao metaDao;

    public List<Board> getList(){
        return boardDao.getList();
    }
    public Board getPostByPostId(int postId){
        return boardDao.getPostByPostId(postId);
    }
    public List<Board> getPostsByPageIdx(PageForm pageForm){
        return boardDao.getPostsByPageIdx(pageForm);
    }
    public int writePost(Board board){
        boardDao.writePost(board);
        metaDao.postIncrement();
        return 0;
    }

    public void editPost(Board board){
        boardDao.editPost(board);
    }

    public void deletePost(int loginId, int postId, int adminLevel){
        DeleteForm deleteForm = new DeleteForm(loginId, postId, adminLevel);
        boardDao.deletePost(deleteForm);
        metaDao.postDecrement();
    }

    public int updateReadCount(int postId){
        return boardDao.updateReadCount(postId);
    }

    public int getPostNum(){
        return metaDao.getPostNum();
    }

    public int getUserNum(){
        return metaDao.getUserNum();
    }
}
