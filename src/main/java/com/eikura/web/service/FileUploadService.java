package com.eikura.web.service;

import com.eikura.web.dao.FileUploadDao;
import com.eikura.web.domain.FileUpload;
import com.eikura.web.util.FileUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class FileUploadService {

    @Resource
    FileUploadDao fileUploadDao;

    @Resource
    FileUtils fileUtils;

    public void uploadFiles(int postId, HttpServletRequest httpServletRequest) throws Exception {

        List<FileUpload> list = fileUtils.parseInsertFileInfo(postId, httpServletRequest);
        if(!list.isEmpty()){
            fileUploadDao.uploadFiles(list);
        }
    }

    public List<String> getImagesByPid(int pid){
        return fileUploadDao.getImagesByPid(pid);
    }


}
