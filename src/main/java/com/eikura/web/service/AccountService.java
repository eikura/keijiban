package com.eikura.web.service;

import com.eikura.web.dao.AccountDao;
import com.eikura.web.dao.MetaDao;
import com.eikura.web.domain.Account;
import com.eikura.web.form.SignUpForm;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AccountService {

    @Resource
    AccountDao accountDao;

    @Resource
    MetaDao metaDao;

    public List<Account> getUsers(){
        return accountDao.getUesrs();
    }
    public int signUp(Account account){
        accountDao.signUp(account);
        metaDao.userIncrement();
        return 0;
    }
    public int deleteAccount(int uId){
        accountDao.deleteAccount(uId);
        metaDao.userDecrement();
        return 0;
    }
    public int getUIdByUserName(String userName){
        return accountDao.getUIdByUserName(userName);
    }
    public String getNickNameByUId(int uId){
        return accountDao.getNickNameByUId(uId);
    }

    public Account getUserInfoByUId(int uId){
        return accountDao.getUserInfoByUId(uId);
    }
}
