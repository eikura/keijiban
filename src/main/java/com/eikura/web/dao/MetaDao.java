package com.eikura.web.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MetaDao {

    @Autowired
    SqlSessionTemplate sqlSessionTemplate;

    public void userIncrement(){
        this.sqlSessionTemplate.update("com.eikura.web.mapper.MetainfoMapper.userIncrement");
    }

    public void userDecrement(){
        this.sqlSessionTemplate.update("com.eikura.web.mapper.MetainfoMapper.userDecrement");
    }

    public void postIncrement(){
        this.sqlSessionTemplate.update("com.eikura.web.mapper.MetainfoMapper.postIncrement");
    }

    public void postDecrement(){
        this.sqlSessionTemplate.update("com.eikura.web.mapper.MetainfoMapper.postDecrement");
    }

    public int getUserNum(){
        return this.sqlSessionTemplate.selectOne("com.eikura.web.mapper.MetainfoMapper.getUserNum");
    }

    public int getPostNum(){
        return this.sqlSessionTemplate.selectOne("com.eikura.web.mapper.MetainfoMapper.getPostNum");
    }
}
