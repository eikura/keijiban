package com.eikura.web.dao;


import com.eikura.web.domain.Board;
import com.eikura.web.form.DeleteForm;
import com.eikura.web.form.PageForm;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BoardDao {

    @Autowired
    SqlSessionTemplate sqlSessionTemplate;

    public List<Board> getList() {
        return this.sqlSessionTemplate.selectList("getList");
    }
    public Board getPostByPostId(int postId){
        return this.sqlSessionTemplate.selectOne("getPostByPostId", postId);
    }
    public List<Board> getPostsByPageIdx(PageForm pageForm){
        return this.sqlSessionTemplate.selectList("getPostsByPageIdx", pageForm);
    }
    public void deletePost(DeleteForm deleteForm){
        this.sqlSessionTemplate.delete("deletePost", deleteForm);
    }
    public int writePost(Board board){
        return this.sqlSessionTemplate.insert("writePost", board);
//        return this.sqlSessionTemplate.selectOne("writePost", board);
    }

    public void editPost(Board board){
        this.sqlSessionTemplate.update("editPost", board);
    }
    public int updateReadCount(int postId){
        return this.sqlSessionTemplate.update("updateReadCount", postId);
    }
}
