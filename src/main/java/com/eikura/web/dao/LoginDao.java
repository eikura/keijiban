package com.eikura.web.dao;


import com.eikura.web.form.LogInForm;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LoginDao {

    @Autowired
    SqlSessionTemplate sqlSessionTemplate;

    public boolean loginCheck(LogInForm logInForm){
        String checkResult = sqlSessionTemplate.selectOne("com.eikura.web.mapper.LoginMapper.loginCheck", logInForm);
        return (checkResult == null) ? false : true;
    }

    public void updateLastLogin(int uId){
        this.sqlSessionTemplate.update("updateLastLogin", uId);
    }
}
