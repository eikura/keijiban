package com.eikura.web.dao;


import com.eikura.web.domain.Account;
import com.eikura.web.form.LogInForm;
import com.eikura.web.form.SignUpForm;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountDao {

    @Autowired
    SqlSessionTemplate sqlSessionTemplate;

    public List<Account> getUesrs() {
        return this.sqlSessionTemplate.selectList("com.eikura.web.mapper.AccountMapper.getUsers");
    }

    public int signUp(Account account){
        return this.sqlSessionTemplate.insert("com.eikura.web.mapper.AccountMapper.signUp", account);
    }

    public int deleteAccount(int uId){
        return this.sqlSessionTemplate.delete("com.eikura.web.mapper.AccountMapper.deleteAccount", uId);
    }

    public int getUIdByUserName(String userName){
        return this.sqlSessionTemplate.selectOne("com.eikura.web.mapper.AccountMapper.getUIdByUserName", userName);
    }

    public String getNickNameByUId(int uId){
        return this.sqlSessionTemplate.selectOne("com.eikura.web.mapper.AccountMapper.getNickNameByUId", uId);
    }
    public int getAdminLevelByUId(int uId){
        return this.sqlSessionTemplate.selectOne("com.eikura.web.mapper.AccountMapper.getAdminLevelByUId", uId);
    }

    public Account getUserInfoByUId(int uId){
        return this.sqlSessionTemplate.selectOne("com.eikura.web.mapper.AccountMapper.getUserInfoByUId", uId);
    }
}
