package com.eikura.web.dao;

import com.eikura.web.domain.FileUpload;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class FileUploadDao {

    @Autowired
    SqlSessionTemplate sqlSessionTemplate;


    public void uploadFiles(List<FileUpload> fileUploadList){
        this.sqlSessionTemplate.insert("com.eikura.web.mapper.FileUploadMapper.uploadFiles", fileUploadList);
    }

    public void deleteFiles(int pid){
        this.sqlSessionTemplate.delete("com.eikura.web.mapper.FileUploadMapper.deleteFiles", pid);
    }

    public List<String> getImagesByPid(int pid){
        return this.sqlSessionTemplate.selectList("com.eikura.web.mapper.FileUploadMapper.getImagesByPid", pid);
    }

}
