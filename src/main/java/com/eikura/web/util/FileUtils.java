package com.eikura.web.util;

import com.eikura.web.domain.FileUpload;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.sql.Timestamp;
import java.util.*;

@Component("fileUtils")
public class FileUtils {

    public List<FileUpload> parseInsertFileInfo(int postId, HttpServletRequest httpServletRequest) throws Exception{
        MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) httpServletRequest;

        List<MultipartFile> files = multipartHttpServletRequest.getFiles(multipartHttpServletRequest.getFileNames().next());

//        MultipartFile multipartFile = null;
        String originalFileName = null;
        String originalFileExtension = null;
        String storedFileName = null;

        List<FileUpload> list = new ArrayList<FileUpload>();
        FileUpload fileUpload = null;

        String filePath = "/Users/user/boardmaven/target/board-maven-1.0-SNAPSHOT/resources/CDN/image/";

        File file = new File(filePath);
        if(!file.exists()){
            file.mkdirs();
        }
//
//        files.stream().forEach( multipartFile -> {
//
//        });
        for(MultipartFile multipartFile : files){
//            multipartFile = multipartHttpServletRequest.getFile(iterator.next());
            if(!multipartFile.isEmpty()){
                originalFileName = multipartFile.getOriginalFilename();
                originalFileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
                if(!isAcceptableImage(originalFileExtension)){
                    continue;
                }
                storedFileName = CommonUtils.getRandomString() + originalFileExtension;

                file = new File(filePath + storedFileName);
                multipartFile.transferTo(file);

                fileUpload = new FileUpload();
                fileUpload.setOriginalFileName(originalFileName);
                fileUpload.setFileSize((int)multipartFile.getSize());
                fileUpload.setStoredFileName(storedFileName);
                fileUpload.setCreaDtm(new Timestamp(System.currentTimeMillis()));
                fileUpload.setPid(postId);
                list.add(fileUpload);
            }
        }
        return list;
    }

    private boolean isAcceptableImage(String extension){
        switch (extension){
            case ".png":
                return true;
            case ".jpg":
                return true;
            case ".jpeg":
                return true;
            case ".tif":
                return true;
            case ".gif":
                return true;
            default:
                return false;

        }
    }
}
