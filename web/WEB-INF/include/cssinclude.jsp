<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Animate.css -->
<link rel="stylesheet" href="/resources/main/css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="/resources/main/css/icomoon.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="/resources/main/css/bootstrap.css">
<!-- Theme style  -->
<link rel="stylesheet" href="/resources/main/css/style.css">
