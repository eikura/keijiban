<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>落書き板</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
        <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
        <meta name="author" content="gettemplates.co" />
    </head>
</head>

<body>
<div class="fh5co-loader"></div>
<nav class="fh5co-nav" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-xs-2">
                <div id="fh5co-logo"><a href="/">落書き板</a></div>
            </div>
            <div class="col-xs-10 text-right menu-1">
                <ul>
                    <li class="active"><a href="<c:url value="/" />">Home</a></li>
                    <li><a href="<c:url value="/board" />">Board</a></li>
                    <c:choose>
                        <c:when test="${sessionScope.userName == null}">
                            <li class="btn-cta"><a href="<c:url value="/login" />"><span>Log in</span></a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="<c:url value="/userInfo" />">My Info</a></li>
                            <li class="btn-cta"><a href="<c:url value="/logout" />"><span>Log out</span></a></li>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>
        </div>
    </div>
</nav>
</body>