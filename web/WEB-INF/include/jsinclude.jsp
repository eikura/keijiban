<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 2019-01-26
  Time: 18:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- jQuery -->
<script src="/resources/main/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="/resources/main/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="/resources/main/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="/resources/main/js/jquery.waypoints.min.js"></script>
<!-- Main -->
<script src="/resources/main/js/main.js"></script>

<!-- Modernizr JS -->
<script src="/resources/main/js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
<script src="/resources/main/js/respond.min.js"></script>
<![endif]-->