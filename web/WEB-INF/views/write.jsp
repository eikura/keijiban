<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>落書き板</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Website Template by gettemplates.co" />
    <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
    <meta name="author" content="gettemplates.co" />

    <!--
    //////////////////////////////////////////////////////

    FREE HTML5 TEMPLATE
    DESIGNED & DEVELOPED by FreeHTML5.co

    Website: 		http://freehtml5.co/
    Email: 			info@freehtml5.co
    Twitter: 		http://twitter.com/fh5co
    Facebook: 		https://www.facebook.com/fh5co

    //////////////////////////////////////////////////////
     -->

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'> -->

    <!-- Animate.css -->
    <link rel="stylesheet" href="/resources/main/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="/resources/main/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="/resources/main/css/bootstrap.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="/resources/main/css/style.css">

    <!-- Modernizr JS -->
    <script src="/resources/main/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="/resources/main/js/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="fh5co-loader"></div>

<div id="page">
    <nav class="fh5co-nav" role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-xs-2">
                    <div id="fh5co-logo"><a href="/">落書き板</a></div>
                </div>
                <div class="col-xs-10 text-right menu-1">
                    <ul>
                        <li class="active"><a href="/">Home</a></li>
                        <li><a href="<c:url value="/board" />">Board</a></li>
                        <c:choose>
                            <c:when test="${sessionScope.userName == null}">
                                <li class="btn-cta"><a href="<c:url value="/login" />"><span>Log in</span></a></li>
                            </c:when>
                            <c:otherwise>
                                <li><a href="<c:url value="/userInfo" />">My Info</a></li>
                                <li class="btn-cta"><a href="<c:url value="/logout" />"><span>Log out</span></a></li>
                            </c:otherwise>
                        </c:choose>
                    </ul>
                </div>
            </div>

        </div>
    </nav>

    <header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(/resources/main/images/img_bg_2.jpg);">
        <%--<div class="overlay"></div>--%>
        <div class="container">
            <div class="row">
                <div class="display-t">
                    <div class="display-tc">
                        <form:form method="post" action="/write/try" modelAttribute="writeAttr" enctype="multipart/form-data">
                        <table class="table table-striped" style="background-color: white">
                            <tr>
                                <td>Title : </td>
                                <td><form:input path="title" /></td>
                            </tr>
                            <tr>
                                <td>Content :</td>
                                <td><form:textarea path="content" /></td>
                            </tr>
                            <tr>
                                <td><input type="submit" value="완료" /></td>
                            </tr>
                        </table>
                            <input type="file" name="file" multiple>
                            <br/><br/>
                        </form:form>

                        <input type="button" class="button" onclick="location.href='/page/1'" value="목록으로" />
                    </div>
                </div>
            </div>
        </div>
    </header>

    <footer id="fh5co-footer" role="contentinfo">
        <div class="container">
            <div class="row copyright">
                <div class="col-md-12 text-center">
                    <p>
                        <small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small>
                        <small class="block">Designed by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images: <a href="http://unsplash.com/" target="_blank">Unsplash</a></small>
                    </p>
                </div>
            </div>

        </div>
    </footer>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>

<!-- jQuery -->
<script src="/resources/main/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="/resources/main/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="/resources/main/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="/resources/main/js/jquery.waypoints.min.js"></script>
<!-- Main -->
<script src="/resources/main/js/main.js"></script>

</body>
</html>




