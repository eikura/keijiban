<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<jsp:include page="../include/cssinclude.jsp" />
	<jsp:include page="../include/header.jsp" />
	<jsp:include page="../include/jsinclude.jsp" />
	<body>
	<div id="page">
	<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(/resources/main/images/img_bg_2.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<h1>日本語に関する様々な他愛のない話の垂れ流し場</h1>
							<h2>ちなみにネットから拾ったテンプレで作られました｡</h2>
							<div class="row">
								<form class="form-inline" id="fh5co-header-subscribe">
									<div class="col-md-8 col-md-offset-2">
										<div class="form-group">
											<%--<form:form method=""--%>
											<%--<select>--%>
<%----%>
<%----%>
											<%--</select>--%>
											<input type="text" class="form-control" id="email" placeholder="Enter your interest (e.g. #Anime, #Japanese)">
											<button type="submit" class="btn btn-default">Search</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<footer id="fh5co-footer" role="contentinfo">
		<div class="container">
			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small> 
						<small class="block">Designed by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images: <a href="http://unsplash.com/" target="_blank">Unsplash</a></small>
					</p>
				</div>
			</div>
		</div>
	</footer>
	</div>

	<%-- 오른쪽 아래에 보이는 맨위로 가는 버튼 --%>
	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	</body>
</html>

