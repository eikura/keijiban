<!DOCTYPE html>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <jsp:include page="../include/header.jsp" flush="false" />

  <head>
    <title>$Title$</title>
  </head>
  <body>

  <c:choose>
      <c:when test="${sessionScope.userName == null}">
          <p>
              <a href="<c:url value="/login" />">로그인</a>
          </p>
          <p>
              <a href="<c:url value="/sign" />">회원가입</a>
          </p>
      </c:when>
      <c:otherwise>
          ${sessionScope.userName}님 안녕하세요.
          <p>
              <a href="<c:url value="/logout" />">로그아웃</a>
              <a href="<c:url value="/write" />">글쓰기</a>
          </p>
      </c:otherwise>
  </c:choose>

  <div>
      <a href="<c:url value="/page/1" />"> 1페이지 </a>
      <a href="<c:url value="/page/2" />"> 2페이지 </a>
  </div>


  <div ng-app="">

    <p>Input something in the input box:</p>
    <p>Name: <input type="text" ng-model="name1"></p>
    <p ng-bind="name1"></p>

  </div>

  <table border="2" width="70%" cellpadding="2">
      <tr><th>Title</th><th>Content</th></tr>
      <c:forEach var="post" items="${posts}" >
          <tr>
              <td>${post.title}</td>
              <td>${post.content}</td>
          </tr>
      </c:forEach>
  </table>
  <h1>=====================</h1>
  <table border="2" width="70%" cellpadding="2">
      <tr><th>Id</th><th>nickName</th><th>Edit</th><th>Delete</th></tr>
      <c:forEach var="user" items="${userList}" >
          <tr>
              <td>${user.UId}</td>
              <td>${user.displayName}</td>
              <td><a href="">Edit</a></td>
              <td><a href="/deleteAccount/${user.UId}">Delete</a></td>
          </tr>
      </c:forEach>
  </table>
  <br />
  <%--<h1>${postList}</h1>--%>
  <%--<h1>=====================</h1>--%>
  <%--<h1>${userList}</h1>--%>

  </body>
</html>
