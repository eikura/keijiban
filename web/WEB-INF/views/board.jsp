<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<jsp:include page="../include/cssinclude.jsp" />
<jsp:include page="../include/header.jsp" />
<jsp:include page="../include/jsinclude.jsp" />

<script>
    function fn_paging(pageIndex) {
        location.href = "/page?pageIndex=" + pageIndex;
    }
</script>

<body>
<%--<div id="page">--%>
    <header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(/resources/main/images/img_bg_2.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="text-center">
                <div class="display-t">
                    <div class="display-tc animate-box" data-animate-effect="fadeIn" >
                        <div>
                            <table class="table table-striped" style="background-color: white">
                                <thead class="thead-default" align="center">
                                <tr>
                                    <th width="10%">Post#</th>
                                    <th width="50%">Title</th>
                                    <th width="10%">Author</th>
                                    <th width="20%">Date</th>
                                    <th width="10%">Read</th>
                                </tr>
                                </thead>
                                <tbody align="left">
                                <c:forEach var="post" items="${posts}" >
                                    <tr>
                                        <td>${post.postId}</td>
                                        <td><a href="<c:url value="/read/${post.postId}"/>" style="color: black">${post.title}</a></td>
                                        <td>${post.displayName}</td>
                                        <td>${post.timeWritten}</td>
                                        <td>${post.readCount}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <div align="left">
                                <button class="btn-sm"><a href="<c:url value="/write" />" class="top-left" style="color: black">글쓰기</a></button>
                            </div>
                                <ul class="pagination">
                                    <%--<input align="top-left" type="button" class="btn btn-primary" onclick="location.href='/write'" value="글쓰기" />--%>
                                    <li>
                                        <a class="btn-group" href="#" onclick="fn_paging('${paging.prevPage }')" aria-label="" style="color: black">
                                            <span aria-hidden="true">이전</span>
                                        </a>
                                    </li>
                                    <c:forEach begin="${minPage}" end="${maxPage}" varStatus="loop" >
                                        <c:choose>
                                            <c:when test="${loop.index==currentPage}">
                                                <li class="active"><a href="#" onclick="fn_paging('${loop.index}')" style="color: black">${loop.index}</a></li>
                                                <%--<a class="btn-group" href="/page/${loop.index}" style="color: black;background-color: #e4b9c0">${loop.index}</a>--%>
                                            </c:when>
                                            <c:otherwise>
                                                <li><a href="#" onclick="fn_paging('${loop.index}')" style="color: gray">${loop.index}</a></li>
                                                <%--<a class="btn-group" href="/page/${loop.index}" style="color: gray">${loop.index}</a>--%>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                    <li>
                                        <a class="btn-group" href="#" onclick="fn_paging('${paging.nextPage }')" aria-label="Next" style="color: black">
                                            <span aria-hidden="true">다음</span>
                                        </a>
                                    </li>
                                </ul>
                            <%--</nav>--%>

                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </header>
<%--</div>--%>
</body>
</html>

