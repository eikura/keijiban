<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<jsp:include page="../include/cssinclude.jsp" />
<jsp:include page="../include/header.jsp" />
<jsp:include page="../include/jsinclude.jsp" />

<script>
    function fn_paging(pageIndex) {
        location.href = "/page?pageIndex=" + pageIndex;
    }
</script>

<body>
<%--<div id="page">--%>
    <header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(/resources/main/images/img_bg_2.jpg);">

        <%--<div class="container" style="margin-top:10%; width: 90%; height: 80%; overflow-y: scroll" >--%>
        <div class="container" style="margin-top:15%; width: 90%; height: 80%" >
            <div class="overlay"></div>
            <div class="row">
                <div class="text-left">
                <div class="display-t">
                    <div class="display-tc animate-box" data-animate-effect="fadeIn" >
                        <div class="display-t">
                        <table class="table table-striped align-content-start" style="background-color: white">
                            <tr style="height: 45px">
                                <th style="width: 20%">글번호</th>
                                <th style="width: 30%">${post.postId}</th>
                                <th style="width: 20%">조회수</th>
                                <th style="width: 30%">${post.readCount}</th>
                            </tr>
                            <tr style="height: 45px">
                                <td>타이틀</td>
                                <td colspan="3">${post.title}</td>
                            </tr>
                            <tr>
                                <td>내용</td>
                                <td colspan="3">
                                    <div style="overflow-y: scroll">
                                        ${post.content}
                                        <br/><br/>
                                        <picture>
                                            <c:forEach items="${images}" var="image">
                                                <a href="${image}"> <img src="${image}" alt="이미지나왔어야함" class="img-thumbnail" height="100px" width="100px"> </a>
                                            </c:forEach>
                                        </picture>
                                    </div>
                                </td>
                            </tr>
                        </table>
                            <input type="button" class="btn-sm btn-light" onclick="fn_paging('${page}')" value="목록으로" />&nbsp;
                            <input type="button" class="btn-sm btn-light" onclick="location.href='https://twitter.com/intent/tweet?via=Keijiban_Project&text=${post.title}&url=${shareURL}'" value="공유하기" />&nbsp;
                            <c:choose>
                                <c:when test="${sessionScope.uId == null}">

                                </c:when>
                                <c:otherwise>
                                    <c:set var="refId" value="${post.UId}" />
                                    <c:set var="compId" value="${sessionScope.uId}" />
                                    <c:if test="${(refId == compId) or (sessionScope.adminLevel > 4)}">
                                        <input type="button" class="btn-sm btn-light" onclick="location.href='/edit/${pid}'" value="수정" />&nbsp;
                                        <input type="button" class="btn-sm btn-light" onclick="location.href='/deletePost/${pid}'" value="삭제" />
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </header>
<%--</div>--%>
</body>
</html>

